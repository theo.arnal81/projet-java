package com.example.projet.residence.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import com.example.projet.residence.domain.Residence;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Appartement {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long residence_id;
    private String name;
    private Integer nb_couchage;
    private Integer surface;
    private boolean equip_baby;
    private boolean climatisation;
    private Integer prix_jour;
    private String type_lieu;



    public String getType_lieu() {
        return type_lieu;
    }

    public void setType_lieu(String type_lieu) {
        this.type_lieu = type_lieu;
    }

    public Integer getPrix_jour() {
        return prix_jour;
    }


    public Long getResidence_id() { return residence_id; }

    public void setResidence_id(Long residence_id) { this.residence_id = residence_id; }

    public String getName() {
        return name;
    }

    public Integer getNb_couchage() {
        return nb_couchage;
    }

    public Integer getSurface(int i) {
        return surface;
    }

    public boolean isEquip_baby() {
        return equip_baby;
    }

    public boolean isClimatisation() {
        return climatisation;
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setNb_couchage(Integer nb_couchage) {
        this.nb_couchage = nb_couchage;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public void setEquip_baby(boolean equip_baby) {
        this.equip_baby = equip_baby;
    }

    public void setClimatisation(boolean climatisation) {
        this.climatisation = climatisation;
    }

    public void setPrix_jour(Integer prix_jour) { this.prix_jour = prix_jour; }





    public Set<com.example.projet.residence.domain.Reservation> getReservation() { return Reservation; }

    public void setReservation(Set<com.example.projet.residence.domain.Reservation> reservation) { Reservation = reservation; }

    @OneToMany
    @JoinColumn(name="APPARTEMENT_ID")
    private Set<Reservation> Reservation;



}
