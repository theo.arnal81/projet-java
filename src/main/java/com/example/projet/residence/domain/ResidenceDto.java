package com.example.projet.residence.domain;

public class ResidenceDto {

    private String name;
    private String pays;
    private String region;
    private boolean wifi;
    private boolean piscine;
    private boolean spa;
    private long nbAppartement;

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isPiscine() {
        return piscine;
    }

    public void setPiscine(boolean piscine) {
        this.piscine = piscine;
    }

    public boolean isSpa() {
        return spa;
    }

    public void setSpa(boolean spa) {
        this.spa = spa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNbAppartement() {
        return nbAppartement;
    }

    public void setNbAppartement(long nbAppartement) {
        this.nbAppartement = nbAppartement;
    }

    public ResidenceDto(String name, long nbAppartement, String pays, String region, boolean wifi, boolean spa, boolean piscine) {
        this.name = name;
        this.pays = pays;
        this.region = region;
        this.piscine = piscine;
        this.wifi = wifi;
        this.spa = spa;
        this.nbAppartement = nbAppartement;
    }

}
