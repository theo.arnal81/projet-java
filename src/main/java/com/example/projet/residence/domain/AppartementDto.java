package com.example.projet.residence.domain;

public class AppartementDto {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNb_couchage() {
        return nb_couchage;
    }

    public void setNb_couchage(Integer nb_couchage) {
        this.nb_couchage = nb_couchage;
    }

    public Float getSurface() {
        return surface;
    }

    public void setSurface(Float surface) {
        this.surface = surface;
    }

    public boolean isEquip_baby() {
        return equip_baby;
    }

    public void setEquip_baby(boolean equip_baby) {
        this.equip_baby = equip_baby;
    }

    public boolean isClimatisation() {
        return climatisation;
    }

    public void setClimatisation(boolean climatisation) {
        this.climatisation = climatisation;
    }

    public void setPrix_jour(Integer prix_jour) {
        this.prix_jour = prix_jour;
    }

    private String name;
    private Integer nb_couchage;
    private Float surface;
    private boolean equip_baby;
    private boolean climatisation;
    private long nbAppartement;
    private Integer prix_jour;

    public String getType_lieu() {
        return type_lieu;
    }

    public void setType_lieu(String type_lieu) {
        this.type_lieu = type_lieu;
    }

    private String type_lieu;

    public Integer getPrix_jour() { return prix_jour; }

    public long getNbAppartement() {
        return nbAppartement;
    }

    public void setNbAppartement(long nbAppartement) {
        this.nbAppartement = nbAppartement;
    }

    public AppartementDto(String name, Integer nb_couchage, Float surface, boolean equip_baby, boolean climatisation, long nbAppartement, Integer prix_jour, String type_lieu) {
        this.name = name;
        this.nb_couchage = nb_couchage;
        this.surface = surface;
        this.equip_baby = equip_baby;
        this.climatisation = climatisation;
        this.nbAppartement = nbAppartement;
        this.prix_jour = prix_jour;
        this.type_lieu = type_lieu;
    }



}
