package com.example.projet.residence.service;

import com.example.projet.residence.domain.*;
import com.example.projet.residence.repository.AppartmentRepository;
import com.example.projet.residence.repository.ReservationRepository;
import com.example.projet.residence.repository.ResidenceRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Service
public class ResidenceService {
    private final AppartmentRepository appartmentRepository;
    private final ResidenceRepository residenceRepository;
    private final ReservationRepository reservationRepository;


    public ResidenceService(AppartmentRepository appartmentRepository, ResidenceRepository residenceRepository, ReservationRepository reservationRepository){
        this.appartmentRepository = appartmentRepository;
        this.residenceRepository =  residenceRepository;
        this.reservationRepository = reservationRepository;
    }

    public Appartement getAppartement(String id) {
        return appartmentRepository.findById(id);
    }
    public Appartement getAppartement(Long id) {
        return appartmentRepository.findById(id).get();
    }

    public Reservation getReservation(String id) {
        return reservationRepository.findById(id);
    }
    public Reservation getReservation(Long id) {
        return reservationRepository.findById(id).get();
    }

    public Residence getResidence(String id) {
        return residenceRepository.findById(id);
    }
    public Residence getResidence(Long id) {
        return residenceRepository.findById(id).get();
    }

    public List<Appartement> getAppartement() {
        return appartmentRepository.findAll();
    }

    //-------------------------------------------------------       CREATE        ----------------------------------------------

    public void CreateAppartement(String nom, Integer surf, boolean clim, boolean equip, Integer nb_couchage, Long id_resid, Integer prix_jour, String type_lieu) {
        Appartement appartement = new Appartement();
        appartement.setName(nom);
        appartement.setSurface(surf);
        appartement.setEquip_baby(equip);
        appartement.setClimatisation(clim);
        appartement.setNb_couchage(nb_couchage);
        appartement.setPrix_jour(prix_jour);
        appartement.setResidence_id(id_resid);
        appartement.setType_lieu(type_lieu);
        appartmentRepository.save(appartement);
    }
    public void CreateResidence(String nom, String pays, boolean piscine, String region, boolean spa, boolean wifi) {
        Residence residence = new Residence();
        residence.setName(nom);
        residence.setPays(pays);
        residence.setPiscine(piscine);
        residence.setRegion(region);
        residence.setSpa(spa);
        residence.setWifi(wifi);
        residenceRepository.save(residence);
    }
    public void CreateReservation(Date date_debut, Date date_fin, Long id_appart ) {
        Reservation reservation = new Reservation();
        reservation.setDate_debut(date_debut);
        reservation.setDate_fin(date_fin);
        reservation.setAppartement_id(id_appart);
        reservationRepository.save(reservation);
    }
    //-------------------------------------------------------       DELETE        --------------------------------------------
    public void DeleteResidence(String id) {
        Residence residence = getResidence(id);
        residenceRepository.delete(residence);
    }
    public void DeleteAppartement(String id) {
        Appartement appartement = getAppartement(id);
        appartmentRepository.delete(appartement);
    }
    public void DeleteReservation(String id) {
        Reservation reservation = getReservation(id);
        reservationRepository.delete(reservation);
    }
    //-------------------------------------------------------       UPDATE        --------------------------------------------
    public void UpdateAppartement(boolean climatisation, boolean equip_baby, Integer nb_couchage, String nom, Integer surface, Long id_resid, Long id) {
       // appartmentRepository.UpdateAppartement(climatisation, equip_baby, nb_couchage, surface, id_resid, id);
        Appartement appartement = getAppartement(id);
        appartement.setClimatisation(climatisation);
        appartement.setEquip_baby(equip_baby);
        appartement.setNb_couchage(nb_couchage);
        appartement.setName(nom);
        appartement.setSurface(surface);
        appartement.setResidence_id(id_resid);
        appartmentRepository.save(appartement);
    }
    public void UpdateResidence(String nom, String pays, boolean piscine, String region, boolean spa, boolean wifi, Long id) {
        Residence residence = getResidence(id);
        residence.setName(nom);
        residence.setPays(pays);
        residence.setPiscine(piscine);
        residence.setRegion(region);
        residence.setSpa(spa);
        residence.setWifi(wifi);
        residenceRepository.save(residence);
    }
    public void UpdateReservation(Long id, Date date_debut, Date date_fin, Long id_appart) {
        Reservation reservation = getReservation(id);
        reservation.setDate_debut(date_debut);
        reservation.setDate_fin(date_fin);
        reservation.setAppartement_id(id_appart);
        reservationRepository.save(reservation);
    }

    public List<Residence> getResidenceByPays(String Pays) {
        return residenceRepository.getResidenceByPays(Pays);
    }

    public List<Residence> getResidenceByRegion(String region) {
        return residenceRepository.getResidenceByRegion(region);
    }

    public List<Residence> getResidenceById(String id) {
        return residenceRepository.getResidenceById(id);
    }

    public List<Reservation> getReservationById(String id) {
        return reservationRepository.getReservationById(id);
    }

    public List<Appartement> getAppartementById(String id){
        return appartmentRepository.getAppartementById(id);
    }

    public List<Appartement> getAppartementsByPiscine() {
        return appartmentRepository.getAppartementsByPiscine();
    }

    public List<Appartement> getAppartementsByType_lieu(String types) {
        return appartmentRepository.getAppartementsByType_lieu(types);
    }

    public List<Reservation> getReservation() {
        return reservationRepository.findAll();
    }

    public List<Appartement> getAppartementLibre() {
        return appartmentRepository.getAppartementLibre();
    }


/*
    public void generateAppartement() {
        Appartement appartement = new Appartement();
        appartement.setName("Appartement 3");
        appartement.setSurface(48);
        appartement.setClimatisation(true);
        appartement.setEquip_baby(false);
        appartement.setNb_couchage(5);
        appartmentRepository.save(appartement);

        appartement = new Appartement();
        appartement.setName("Appartement 4");
        appartement.setSurface(100);
        appartement.setClimatisation(true);
        appartement.setEquip_baby(true);
        appartement.setNb_couchage(2);
        appartmentRepository.save(appartement);
    }


    public void generateResidence() {
        Residence residence = new Residence();
        residence.setName("Residence 1");
        residence.setPays("France");
        residence.setRegion("Tarn");
        residence.setSpa(true);
        residence.setWifi(false);
        residence.setPiscine(true);
        residenceRepository.save(residence);
        residence.setAppartements(new HashSet<>());
        Appartement appartement = new Appartement();
        appartement.setName("Appartement 10");
        appartement.setNb_couchage(1);
        appartement.setClimatisation(true);
        appartement.setEquip_baby(true);
        appartement.setSurface(45);
        appartmentRepository.save(appartement);
        residence.getAppartements().add(appartement);
        appartement = new Appartement();
        appartement.setName("Appartement 11");
        appartement.setNb_couchage(2);
        appartement.setClimatisation(false);
        appartement.setEquip_baby(true);
        appartement.setSurface(85);
        appartmentRepository.save(appartement);
        residence.getAppartements().add(appartement);
        residenceRepository.save(residence);
    }
*/

}
