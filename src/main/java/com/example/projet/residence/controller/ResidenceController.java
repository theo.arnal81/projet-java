package com.example.projet.residence.controller;

import com.example.projet.residence.domain.Appartement;
import com.example.projet.residence.domain.Reservation;
import com.example.projet.residence.domain.Residence;
import com.example.projet.residence.service.ResidenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("")
public class ResidenceController {

    private Logger LOGGER = LoggerFactory.getLogger(ResidenceController.class);


    ResidenceService residenceService;
    public ResidenceController(ResidenceService residenceService) {
        this.residenceService = residenceService;
    }


    @GetMapping("liste des appartements")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement> getAppartement(){
        return residenceService.getAppartement();
    }

    @GetMapping("Liste des reservations")
    @ResponseStatus(HttpStatus.OK)
    public List<Reservation> getReservation() { return residenceService.getReservation(); }

    //-------------------------------------------------------       RECHERCHE       -------------------------------------------------------
    @GetMapping("Recherche de residence par Pays")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> getResidenceByPays(String Pays){
        return residenceService.getResidenceByPays(Pays);
    }
    @GetMapping("Recherche Appartement avec une Piscine")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement> getAppartementsByPiscine() {
        return residenceService.getAppartementsByPiscine();
    }
    @GetMapping("Recherche d'Appartement par type de lieux")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement> getAppartementsByType_lieu(String types) {
        return residenceService.getAppartementsByType_lieu(types);
    }
    @GetMapping("Recherche de residence par Region")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> getResidenceByRegion(String region) {
        return residenceService.getResidenceByRegion(region);
    }
    @GetMapping("Recherce d'appartement libre entre : 2020-05-02 et 2020-06-01")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement> getAppartementLibre() {
        return residenceService.getAppartementLibre();
    }

    //-------------------------------------------------------       Recherce Par ID        ----------------------------------------------
    @GetMapping("Rechercher un Appartement par ID")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement> getAppartementById(String id) {
        return residenceService.getAppartementById(id);
    }
    @GetMapping("Rechercher un Residence par ID")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> getResidenceById(String id) {
        return residenceService.getResidenceById(id);
    }
    @GetMapping("Recherche une Reservation pas ID")
    @ResponseStatus(HttpStatus.OK)
    public List<Reservation> getReservationById(String id) {
        return residenceService.getReservationById(id);
    }
    //-------------------------------------------------------       CREATION         -----------------------------------------------------
    @PutMapping("Creation de Residence")
    @ResponseStatus(HttpStatus.OK)
    public void CreateResidence(String nom, String pays, boolean piscine, String region, boolean spa, boolean wifi) {
        residenceService.CreateResidence(nom, pays, piscine, region, spa, wifi);
    }
    @PutMapping("/Create_Appartement")
    @ResponseStatus(HttpStatus.OK)
    public void CreateAppartement(String nom, Integer surf, boolean clim, boolean equip, Integer nb_couchage, String id, Long id_resid, Integer prix_jour, String type_lieu){
        residenceService.CreateAppartement(nom, surf, clim, equip, nb_couchage, id_resid, prix_jour, type_lieu);
    }
    @PutMapping("Create Reservation")
    @ResponseStatus(HttpStatus.OK)
    public void CreateReservation(Date date_debut, Date date_fin, Long id_appart ) {
        residenceService.CreateReservation(date_debut, date_fin, id_appart);
    }
    //-------------------------------------------------------       UPDATE         ------------------------------------------------------
    @PostMapping("Update_Appartement")
    @ResponseStatus(HttpStatus.OK)
    public void UpdateAppartement(Boolean climatisation, Boolean equip_baby, Integer nb_couchage, String nom, Integer surface, Long id_resid, Long id) {
        residenceService.UpdateAppartement(climatisation, equip_baby, nb_couchage, nom, surface, id_resid, id);
        LOGGER.info(climatisation + " " + equip_baby + " " + nb_couchage + ' ' + nom + ' ' + surface + ' ' + id_resid + ' ' + id);
    }
    @PostMapping("Update de residence")
    @ResponseStatus(HttpStatus.OK)
    public void UpdateResidence(String nom, String pays, boolean piscine, String region, boolean spa, boolean wifi, Long id) {
        residenceService.UpdateResidence(nom, pays, piscine, region, spa, wifi, id);
    }
    @PostMapping("Update Reservation")
    @ResponseStatus(HttpStatus.OK)
    public void UpdateReservation(Long id, Date date_debut, Date date_fin, Long id_appart) {
        residenceService.UpdateReservation(id, date_debut, date_fin, id_appart);
    }
    //-------------------------------------------------------       DELETE        -------------------------------------------------------
    @DeleteMapping("Suppresion d'appartement")
    @ResponseStatus(HttpStatus.OK)
    public void DeleteAppartement(String id) {
        residenceService.DeleteAppartement(id);
    }
    @DeleteMapping("Suppresion de Reservation")
    @ResponseStatus(HttpStatus.OK)
    public void DeleteReservation(String id) {
        residenceService.DeleteReservation(id);
    }
    @DeleteMapping("Suppresion de Residence")
    @ResponseStatus(HttpStatus.OK)
    public void DeleteResidence(String id) {
        residenceService.DeleteResidence(id);
    }




/*

    @GetMapping("/appartement/generate")
    @ResponseStatus(HttpStatus.OK)
    public void generateAppartement() {
        residenceService.generateAppartement();
    }

    @GetMapping("/residence/generate")
    @ResponseStatus(HttpStatus.OK)
    public void generateResidence() {
        residenceService.generateResidence();
    }


*/


}
