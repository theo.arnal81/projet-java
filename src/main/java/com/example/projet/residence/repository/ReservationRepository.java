package com.example.projet.residence.repository;

import com.example.projet.residence.domain.Reservation;
import com.example.projet.residence.domain.Residence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    Reservation findById(String id);

    @Query(value = "SELECT * FROM reservation WHERE id like :id", nativeQuery = true )
    List<Reservation> getReservationById(String id);

    @Query(value = "SELECT * FROM appartement JOIN reservation reser WHERE reser.date_debut BETWEEN '2020-04-01' AND '2020-06-01' ", nativeQuery = true)
    List<Reservation> getReservationByDate_debut();
}
