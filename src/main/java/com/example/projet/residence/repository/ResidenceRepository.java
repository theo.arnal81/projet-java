package com.example.projet.residence.repository;

import com.example.projet.residence.domain.Appartement;
import com.example.projet.residence.domain.AppartementDto;
import com.example.projet.residence.domain.Residence;
import com.example.projet.residence.domain.ResidenceDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResidenceRepository extends JpaRepository<Residence, Long> {
    Residence findById(String id);

    @Query(value = "SELECT * from residence WHERE pays LIKE lower(:Pays)", nativeQuery = true)
    List<Residence> getResidenceByPays(String Pays);

    @Query(value = "SELECT * from residence WHERE region LIKE lower(:region)", nativeQuery = true)
    List<Residence> getResidenceByRegion(String region);

    @Query(value = "SELECT * FROM residence WHERE id like :id", nativeQuery = true )
    List<Residence> getResidenceById(String id);
}
