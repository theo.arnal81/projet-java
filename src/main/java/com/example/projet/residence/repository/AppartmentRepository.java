package com.example.projet.residence.repository;

import com.example.projet.residence.domain.Appartement;
import com.example.projet.residence.domain.AppartementDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppartmentRepository extends JpaRepository<Appartement, Long> {
    Appartement findById(String id);
/*
    @Query(value = "INSERT Into appartement (climatisation, equip_baby, name, nb_couchage, surface, residence_id ) VALUES (:clim, :equip, :nom, :nb_couchage, :surf, :Id_resid ) ", nativeQuery = true)
    Appartement CreateAppartement(String nom, Integer surf, boolean clim, boolean equip, Integer nb_couchage, Integer Id_resid );
*/
    @Query(value = "SELECT * FROM appartement WHERE id like :id", nativeQuery = true )
    List<Appartement> getAppartementById(String id);

    @Query(value = "SELECT * FROM appartement JOIN residence r ON appartement.residence_id = r.id where r.piscine = 1", nativeQuery = true)
    List<Appartement> getAppartementsByPiscine();

    @Query(value = "SELECT * FROM appartement WHERE type_lieu like lower(:types)", nativeQuery = true )
    List<Appartement> getAppartementsByType_lieu(String types);

    @Query(value = "SELECT * FROM appartement JOIN reservation reser ON appartement.id = reser.appartement_id WHERE (reser.date_debut NOT BETWEEN '2020-05-02' AND '2020-05-03') " +
            "AND (reser.date_fin NOT BETWEEN '2020-05-29' AND '2020-06-01')", nativeQuery = true)
    List<Appartement> getAppartementLibre();


}
